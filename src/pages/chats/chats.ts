import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Events, AlertController } from 'ionic-angular';
import { RequestsProvider } from '../../providers/requests/requests';

/**
 * Generated class for the ChatsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-chats',
  templateUrl: 'chats.html',
})
export class ChatsPage {

  myrequests;
  myfriends;

  constructor(public navCtrl: NavController, public navParams: NavParams,
              public requestservice: RequestsProvider, public events: Events,
              public alertCtrl: AlertController) {
  }

  ionViewWillEnter() {
    this.requestservice.getmyrequests();
    this.requestservice.getmyfriends();
    this.myfriends = [];
    this.events.subscribe('gotrequests', () => {
      this.myrequests = [];
      this.myrequests = this.requestservice.userdetails;
    })
    this.events.subscribe('friends', () => {
      this.myfriends = [];
      this.myfriends = this.requestservice.myfriends; 
    })
  }

  ionViewDidLeave() {
    this.events.unsubscribe('gotrequests');
    this.events.unsubscribe('friends');
  }

  addbuddy() {
    this.navCtrl.push('BuddiesPage');
  }

  accept(item: any) {
    this.requestservice.acceptrequest(item).then((res: any) => {
      let newalert = this.alertCtrl.create({
        buttons: ['Okay']
      });
      if(res.succes) {
        newalert.setTitle('Friend Add');
        newalert.setSubTitle('Now you are friends');
        newalert.present();
      } else {
        newalert.setTitle('Something was wrong');
        newalert.setSubTitle('Error in Accept Request' + res);
        newalert.present();
      }
    }).catch((err) => {

    })
  }

  ignore(item: any) {
    this.requestservice.deleterequest(item).then((res) => {

    }).catch((err) => {

    })
  }

}
