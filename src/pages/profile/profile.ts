import { Component, NgZone } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import { ImghandlerProvider } from '../../providers/imghandler/imghandler';
import { UserProvider } from '../../providers/user/user';
import { AuthProvider } from '../../providers/auth/auth';

/**
 * Generated class for the ProfilePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-profile',
  templateUrl: 'profile.html',
})
export class ProfilePage {

  avatar: string;
  displayName: string;

  constructor(public navCtrl: NavController, public navParams: NavParams, 
              public userservice: UserProvider, public zone: NgZone, 
              public authservice: AuthProvider, public alertCtrl: AlertController,
              public imghandler: ImghandlerProvider) {
  }

  ionViewWillEnter() {
    this.loaduserdetails();
  }

  loaduserdetails() {
    this.userservice.getuserdetails().then((res: any) => {
        this.displayName = res.displayName;
        this.zone.run(() => {
          this.avatar = res.photoURL;
        })
    });
  }

  logout() { 
    this.authservice.logout().then((res: any) => {
      if(res.succes) {
        this.navCtrl.setRoot('LoginPage');
      } else {
        alert('Something missing');
      }
    });
  }

  editimage() { 
    let statusalert = this.alertCtrl.create({
      buttons: ['Okay']
    });
     this.imghandler.uploadimage().then((url: any) => {
       this.userservice.updateimage(url).then((res: any) => {
         if(res.succes) { 
          statusalert.setTitle('Updated');
          statusalert.setSubTitle('Your profile pic has been changed successfully!!');
          statusalert.present();
          this.zone.run(() => {
          this.avatar = url;
          }); 
         } else {
          statusalert.setTitle('Failed');
          statusalert.setSubTitle('Your profile pic was not changed');
          statusalert.present();
         }
       })
     })
  }

  editname() {
    let statusalert = this.alertCtrl.create({
      buttons: ['Okay']
    });

    let alert = this.alertCtrl.create({
      title: 'Edit Nickname',
      inputs: [{
        name: 'nickname',
        placeholder: 'Nickname'
      }],
      buttons: [{
        text: 'Cancel',
        role: 'cancel',
        handler: data => {

        }
      },
      {
        text: 'Edit',
        handler: data => {
          if(data.nickname) {
          this.userservice.updatedisplayname(data.nickname).then((res: any) => {
            if (res.succes) {
              statusalert.setTitle('Updated');
              statusalert.setSubTitle('Your nickname has been changed successfully!!');
              statusalert.present();
              this.zone.run(() => {
                this.displayName = data.nickname;
              })
            }
            else {
              statusalert.setTitle('Failed');
              statusalert.setSubTitle('Your nickname was not changed');
              statusalert.present();
            }
          })
          }
        }
      }]
    });

    alert.present();
  }



}
