import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AgilePage } from './agile';

@NgModule({
  declarations: [
    AgilePage,
  ],
  imports: [
    IonicPageModule.forChild(AgilePage),
  ],
})
export class AgilePageModule {}
