import { Injectable } from '@angular/core';
import firebase from 'firebase';
import { connreq } from '../../models/interfaces/request';
import { UserProvider } from '../user/user';
import { Events } from 'ionic-angular';

/*
  Generated class for the RequestsProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class RequestsProvider {

  firereq = firebase.database().ref('/requests');  
  firefriends = firebase.database().ref('/friends');
  userdetails;
  myfriends;

  constructor(public userservice: UserProvider, public events: Events) {
    console.log('Hello RequestsProvider Provider');
  }

  sendrequest(req: connreq) {
    return new Promise((resolve, reject) => {
      this.firereq.child(req.recipient).push({
      sender: req.sender
      }).then((res) => {        
        resolve({ success: true });
        },
        (err) => {
          reject(err);
        })
    });
  }

  getmyrequests() {
    let allmyrequests;
    let myrequests = [];
    
      this.firereq.child(firebase.auth().currentUser.uid).once('value', (snapshot) => {
          allmyrequests = snapshot.val();          
          myrequests = [];
          for(var i in allmyrequests) {
            myrequests.push(allmyrequests[i].sender);
          }
          this.userservice.getallusers().then((res) => {
            let allusers = res;
            this.userdetails = [];
            for(var j in myrequests) 
              for(var key in allusers) {
                if(myrequests[j] == allusers[key].uid) {
                    this.userdetails.push(allusers[key]);
                }
              }
              this.events.publish('gotrequests');
          })          
      })
  }

  getmyfriends() {
    let friendsuid = [];
    this.firefriends.child(firebase.auth().currentUser.uid).once('value', (snapshot) => {
      let allfriends = snapshot.val();
      this.myfriends = [];
      for (var i in allfriends)
        friendsuid.push(allfriends[i].uid);
        
      this.userservice.getallusers().then((users) => {
        this.myfriends = [];
        for (var j in friendsuid)
          for (var key in users) {
            if (friendsuid[j] === users[key].uid) {
              this.myfriends.push(users[key]);
            }
          }
        this.events.publish('friends');
      }).catch((err) => {
        alert(err);
      })
    
    })
  }

  acceptrequest(buddy) {
      return new Promise((resolve, reject) => {
          this.firefriends.child(firebase.auth().currentUser.uid).push({
            uid: buddy.uid
          }).then((res) => { 
            if(res) {              
              this.firefriends.child(buddy.uid).push({
                uid: firebase.auth().currentUser.uid
              }).then((res) => {
                if(res) {
                this.deleterequest(buddy).then(() => {
                  resolve({succes: true});
                }).catch((err) => {
                  reject(err);
                })
              }
          },
          (err) => { reject(err)}
        )
        }
          },
        (err) => {
          reject(err);
        })
      });
  }

  deleterequest(buddy) {
    return new Promise((resolve, reject) => {
      this.firereq.child(firebase.auth().currentUser.uid)
      .orderByChild('sender').equalTo(buddy.uid).once('value', (snapshot) => {
        let dataset = snapshot.val();
        let somekey;
        for (var key in dataset)
          somekey = key;
          this.firereq.child(firebase.auth().currentUser.uid).child(somekey).remove().then(() => {
          resolve(true);
          })
      }).then(() => {
        
      }).catch((err) => {
        reject(err);
      })
    });
  }

}
