import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

import { AngularFireAuth} from 'angularfire2/auth';
import firebase from 'firebase';

import { newuser} from '../../models/interfaces/newuser';

/*
  Generated class for the UserProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class UserProvider {

  firedata = firebase.database().ref('/users');
  constructor(public afireauth: AngularFireAuth) {
    console.log('Hello UserProvider Provider');
  }

  adduser(newuser: newuser) {
    var promise = new Promise((resolve, reject) => {
      this.afireauth.auth.createUserWithEmailAndPassword(newuser.email, newuser.password).then(() => {
        this.afireauth.auth.currentUser.updateProfile({
          displayName: newuser.displayName,
          photoURL: 'https://firebasestorage.googleapis.com/v0/b/myapp-4eadd.appspot.com/o/chatterplace.png?alt=media&token=e51fa887-bfc6-48ff-87c6-e2c61976534e'
        }).then(() => {
          this.firedata.child(this.afireauth.auth.currentUser.uid).set({
            uid: this.afireauth.auth.currentUser.uid,
            displayName: newuser.displayName,
            photoURL: 'https://firebasestorage.googleapis.com/v0/b/myapp-4eadd.appspot.com/o/chatterplace.png?alt=media&token=e51fa887-bfc6-48ff-87c6-e2c61976534e'
          }).then(() => {
            resolve({succes: true})
          }).catch((err) => {
            reject(err);
        })
        }).catch((err) => {
          reject(err);
        })
      }).catch((err) => {
        reject(err);
      })
    });

    return promise;
  }

  passwordreset(email) {
    var promise = new Promise((resolve, reject) => {
        this.afireauth.auth.sendPasswordResetEmail(email).then(() => {
          resolve({ success: true });
        }).catch((err) => {
          reject(err);
        })
    });

    return promise;
  }

  updateimage(imageurl) {
    var promise = new Promise((resolve, reject) => {
        this.afireauth.auth.currentUser.updateProfile({
          displayName: firebase.auth().currentUser.displayName,
          photoURL: imageurl
        }).then(() => {
          this.firedata.child(firebase.auth().currentUser.uid).update({
            displayName: firebase.auth().currentUser.displayName,
            photoURL: imageurl
          }).then(() => {
            resolve({ succes: true});
          }).catch((err) => {
            reject(err);
          })
        }).catch((err) => {
          reject(err);
        })
    });

    return promise;
  }

  getuserdetails() {
    return new Promise((resolve, reject) => {
        this.firedata.child(firebase.auth().currentUser.uid).once('value', (snapshot) => {
          resolve(snapshot.val());
        }).catch((err) => {
          reject(err);
        });
     });     
  }

  updatedisplayname(newname) {
    return new Promise((resolve, reject) => {
        this.afireauth.auth.currentUser.updateProfile({
          displayName: newname,
          photoURL: firebase.auth().currentUser.photoURL
        }).then(() => {
          this.firedata.child(firebase.auth().currentUser.uid).update({
            displayName: newname,
            photoURL: firebase.auth().currentUser.photoURL,
            uid: firebase.auth().currentUser.uid
          }).then(() => {
            resolve({succes: true});
          }).catch((err) => {
            reject(err);
          })
        }).catch((err) => {
          reject(err);
        })
    });
  }

  getallusers() {
    return new Promise((resolve, reject) => {
      this.firedata.orderByChild('uid').once('value' , (snapshot) => {
        let userdata = snapshot.val();
        let temparr = [];
        for (var key in userdata) {
          temparr.push(userdata[key]);
        }
        resolve(temparr);
      }).catch((err) => {
        reject(err);
      })
    });
  }

}
